package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public abstract class CheckoutBasePage extends BasePage {
    @FindBy(xpath = "//button/span[text()='Back']")
    WebElement backBtn;
    @FindBy(xpath = "//button/span[text()='Next']")
    WebElement nextBtn;

    public CheckoutBasePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public abstract <T> T clickNext();
}