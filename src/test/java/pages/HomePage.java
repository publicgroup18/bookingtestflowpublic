package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {
    public static final String URL = "https://www.booking.com/";
    public HeaderPageElement headerPage;

    public HomePage(WebDriver driver, HeaderPageElement header){
        super(driver);
        PageFactory.initElements(driver, this);
        headerPage = header;
    }
}
