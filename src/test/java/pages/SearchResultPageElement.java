package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class SearchResultPageElement extends BasePage {

    public SearchResultPageElement(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public FlightDetailsPage seeFlight(int index) {
        String xpath = String.format("(//*[@id='flight-card']//button)[%s]", Integer.toString(index));
        waitHelper.waitForClickable(By.xpath(xpath));
        driver.findElement(By.xpath(xpath)).click();
        return new FlightDetailsPage(driver);
    }

}
