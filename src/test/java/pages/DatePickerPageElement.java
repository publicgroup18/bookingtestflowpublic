package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DatePickerPageElement extends BasePage {
    @FindBy(xpath = "(//*[contains(@data-testid, 'input_date_range')]//button[contains(@class, 'Actionable-module')])[1]")
    private WebElement prevMonthBtn;
    @FindBy(xpath = "(//*[contains(@data-testid, 'input_date_range')]//button[contains(@class, 'Actionable-module')])[2]")
    private WebElement nextMonthBtn;

    public DatePickerPageElement(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public DatePickerPageElement clickNextMonthBtn() {
        nextMonthBtn.click();
        return this;
    }
    public DatePickerPageElement selectDay(int day, int index) {
        driver.findElement(By.xpath(String.format("(//td[@role='gridcell']//span[text()='%s'])[%s]", Integer.toString(day), Integer.toString(index)))).click();
        return this;
    }
    public DatePickerPageElement selectFromDay(int day) {
        return selectDay(day, 1);
    }
    public FlightsPage selectToDay(int day) {
        selectDay(day, 1);
        return new FlightsPage(driver, new HeaderPageElement(driver));
    }
}
