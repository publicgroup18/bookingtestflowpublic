package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SearchAirportPageElement extends BasePage {
    @FindAll({@FindBy(xpath = "//*[contains(@class,'DismissibleContainer')]/div//*[contains(@class,'Stack-module')]//*[contains(@class, 'Icon')]")})
    private List<WebElement> selectedFlightsCloseIcon;
    @FindAll({@FindBy(xpath = "//div[@data-testid='autocomplete_result']")})
    private List<WebElement> foundAirports;
    @FindBy(xpath = "//input[contains(@placeholder, 'Where') and contains(@data-testid, 'searchbox')]")
    private WebElement searchAirportInput;

    public SearchAirportPageElement(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public SearchAirportPageElement clearSelectedFlights() {
        for (WebElement element : selectedFlightsCloseIcon) {
            element.click();
        }
        return this;
    }

    public SearchAirportPageElement searchForAirport(String text) {
        waitHelper.waitForVisible(searchAirportInput);
        searchAirportInput.sendKeys(text);
        return this;
    }

    public SearchAirportPageElement selectFoundAirport(int index) {
        WebElement element = foundAirports.get(index);
        element.click();
        waitHelper.waitForInvisible(element);
        return new SearchAirportPageElement(driver);
    }

    public SearchAirportPageElement selectFoundAirport() {
        return selectFoundAirport(0);
    }

    public FlightsPage selectFoundTargetAirport() {
        selectFoundAirport();
        return new FlightsPage(driver, new HeaderPageElement(driver));
    }
}
