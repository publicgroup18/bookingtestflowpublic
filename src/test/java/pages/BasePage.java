package pages;

import helpers.WaitHelper;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.Duration;

public class BasePage {
    @FindBy(css = "button#onetrust-accept-btn-handler")
    WebElement acceptCockies;
    WebDriver driver;
    WaitHelper waitHelper;
    public BasePage(WebDriver driver){
        this.driver = driver;
        waitHelper = new WaitHelper(driver);
    }

    public void clickAcceptCockies(){
        acceptCockies.click();
    }

    public void assertContainsText(WebElement element, String text) {
        Assertions.assertTrue(element.getText().contains(text) || element.getAttribute("value").contains(text));
    }

}
