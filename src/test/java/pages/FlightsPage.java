package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FlightsPage extends BasePage {
    @FindBy(xpath = "(//*[contains(@class, 'Flyout-module')]/div[@role='button'])[1]")
    private WebElement fromAirport;
    @FindBy(xpath = "(//*[contains(@class, 'Flyout-module')]/div[@role='button'])[2]")
    private WebElement toAirport;
    @FindBy(xpath = "//input[@placeholder='Depart']")
    private WebElement departDateBtn;
    @FindBy(xpath = "//input[@placeholder='Return']")
    private WebElement returnDateBtn;
    @FindBy(xpath = "//span[contains(@class, 'InputCheckbox-module__field')]")
    private WebElement directFlightsOnly;
    @FindBy(xpath = "//button[text()='Search']")
    private WebElement searchBtn;
    public static final String URL = "https://www.booking.com/flights/";
    public HeaderPageElement headerPage;

    public FlightsPage(WebDriver driver, HeaderPageElement header) {
        super(driver);
        PageFactory.initElements(driver, this);
        waitHelper.waitForTitleContains("Flights");
        headerPage = header;
    }

    public SearchAirportPageElement clickFromAirport() {
        waitHelper.waitForClickable(fromAirport);
        fromAirport.click();
        return new SearchAirportPageElement(driver);
    }

    public FlightsPage clickDirectFlightsOnly() {
        directFlightsOnly.click();
        return this;
    }

    public SearchResultPageElement clickSearchButton() {
        searchBtn.click();
        return new SearchResultPageElement(driver);
    }

    public DatePickerPageElement clickDepart() {
        departDateBtn.click();
        return new DatePickerPageElement(driver);
    }

    public FlightsPage assertFromAirportContainsText(String text) {
        assertContainsText(fromAirport, text);
        return this;
    }

    public FlightsPage assertToAirportContainsText(String text) {
        assertContainsText(toAirport, text);
        return this;
    }

    public FlightsPage assertDepartDateContainsText(int day) {
        assertContainsText(departDateBtn, Integer.toString(day));
        return this;
    }

    public FlightsPage assertReturnDateContainsText(int day) {
        assertContainsText(returnDateBtn, Integer.toString(day));
        return this;
    }
}
