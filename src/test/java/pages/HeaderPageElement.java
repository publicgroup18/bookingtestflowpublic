package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderPageElement extends BasePage {
    @FindBy(xpath = "//span[contains(text(), 'Flights')]")
    private WebElement flights;

    public HeaderPageElement(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public FlightsPage clickFlights() {
        waitHelper.waitForClickable(flights);
        flights.click();
        return new FlightsPage(driver, this);
    }
}
