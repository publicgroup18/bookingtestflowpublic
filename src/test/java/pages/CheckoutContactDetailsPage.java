package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutContactDetailsPage extends CheckoutBasePage {
    @FindBy(xpath = "//input[@type='email']")
    private WebElement email;
    @FindBy(xpath = "//input[@type='tel']")
    private WebElement phone;
    @FindBy(xpath = "//input[@type='checkbox']")
    private WebElement sendSmsChkbx;
    @FindBy(xpath = "//input[contains(@name, 'firstName')]")
    private WebElement firstName;
    @FindBy(xpath = "//input[contains(@name, 'lastName')]")
    private WebElement lastName;
    @FindBy(xpath = "//select[contains(@name, 'gender')]")
    private WebElement gender;
    @FindBy(xpath = "//input[@placeholder='DD']")
    private WebElement birthDay;
    @FindBy(xpath = "//select[contains(@name, 'birthDate')]")
    private WebElement birthMonth;
    @FindBy(xpath = "//input[@placeholder='YYYY']")
    private WebElement birthYear;

    private String emailValue;
    private String phoneValue;
    private String fnValue;
    private String lnValue;
    private String genderValue;
    private String dayValue;
    private String monthValue;
    private String yearValue;


    public CheckoutContactDetailsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
        waitHelper.waitForTitleContains("Who's flying?");
    }

    public CheckoutContactDetailsPage setEmail(String email) {
        waitHelper.waitForVisible(this.email);
        this.email.sendKeys(email);
        emailValue = email;
        return this;
    }

    public CheckoutContactDetailsPage setPhone(String phone) {
        this.phone.sendKeys(phone);
        phoneValue = phone;
        return this;
    }
    public CheckoutContactDetailsPage setGetFreeTextMessage() {
        this.sendSmsChkbx.click();
        return this;
    }

    public CheckoutContactDetailsPage setFirstName(String fName) {
        this.firstName.sendKeys(fName);
        fnValue = fName;
        return this;
    }

    public CheckoutContactDetailsPage setLastName(String lName) {
        this.lastName.sendKeys(lName);
        lnValue = lName;
        return this;
    }

    public CheckoutContactDetailsPage selectGender(String gender) {
        this.gender.sendKeys(gender);
        genderValue = gender;
        return this;
    }

    public CheckoutContactDetailsPage setDayOfBirth(String dd) {
        this.birthDay.sendKeys(dd);
        dayValue = dd;
        return this;
    }

    public CheckoutContactDetailsPage selectMonthOfBirth(String mm) {
        this.birthMonth.sendKeys(mm);
        monthValue = mm;
        return this;
    }

    public CheckoutContactDetailsPage setYearOfBirth(String yyyy) {
        this.birthYear.sendKeys(yyyy);
        yearValue = yyyy;
        return this;
    }

    public Object clickNext() {
        throw new RuntimeException("Implementation required");
    }

    public CheckoutFarePage clickBack() {
        backBtn.click();
        return new CheckoutFarePage(driver);
    }

    public CheckoutContactDetailsPage assertEmailText() {
        assertContainsText(email, emailValue);
        return this;
    }
    public CheckoutContactDetailsPage assertPhoneText() {
        assertContainsText(phone, phoneValue);
        return this;
    }
    public CheckoutContactDetailsPage assertFirstNameText() {
        assertContainsText(firstName, fnValue);
        return this;
    }
    public CheckoutContactDetailsPage assertLastNameText() {
        assertContainsText(lastName, lnValue);
        return this;
    }
    public CheckoutContactDetailsPage assertGenderText() {
        assertContainsText(gender, genderValue);
        return this;
    }
    public CheckoutContactDetailsPage assertDayText() {
        assertContainsText(birthDay, dayValue);
        return this;
    }
    public CheckoutContactDetailsPage assertMonthText() {
        assertContainsText(birthMonth, monthValue);
        return this;
    }
    public CheckoutContactDetailsPage assertYearText() {
        assertContainsText(birthYear, yearValue);
        return this;
    }
}
