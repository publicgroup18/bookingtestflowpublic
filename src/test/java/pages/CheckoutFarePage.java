package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutFarePage extends CheckoutBasePage{
    public CheckoutFarePage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public CheckoutContactDetailsPage clickNext(){
        waitHelper.waitForClickable(nextBtn);
        nextBtn.click();
        return new CheckoutContactDetailsPage(driver);
    }

    public FlightDetailsPage clickBack() {
        backBtn.click();
        return new FlightDetailsPage(driver);
    }

}
