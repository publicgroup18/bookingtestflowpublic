package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FlightDetailsPage extends BasePage{
    @FindBy(xpath = "//button/span[text()='Select']")
    private WebElement selectBtn;
    public FlightDetailsPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public CheckoutFarePage clickSelect(){
        waitHelper.waitForClickable(selectBtn);
        selectBtn.click();
        return new CheckoutFarePage(driver);
    }
}
