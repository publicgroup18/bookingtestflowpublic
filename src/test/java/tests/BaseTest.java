package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.HeaderPageElement;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    String driverPath = System.getProperty("user.dir") + "\\chromedriver.exe";
    WebDriver driver;
    HomePage homepage;
    public BaseTest(){
        System.setProperty("webdriver.chrome.driver", driverPath);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(HomePage.URL);
        driver.manage().window().maximize();
        homepage = new HomePage(driver, new HeaderPageElement(driver));
    }

}
