package tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import pages.HeaderPageElement;
import pages.HomePage;

public class BookingTests extends BaseTest {
    @Test
    public void BookingTest() {
        HomePage homePage = new HomePage(driver, new HeaderPageElement(driver));
        homePage.clickAcceptCockies();
        homePage.headerPage.clickFlights().
                clickFromAirport().
                clearSelectedFlights().
                searchForAirport("AMS").
                selectFoundAirport().
                searchForAirport("NYC").
                selectFoundTargetAirport().
                clickDepart().
                clickNextMonthBtn().
                clickNextMonthBtn().
                selectFromDay(1).
                selectToDay(28).
                clickDirectFlightsOnly().
                assertFromAirportContainsText("AMS").
                assertToAirportContainsText("New York").
                assertDepartDateContainsText(1).
                assertReturnDateContainsText(28).
                clickSearchButton().
                seeFlight(2).
                clickSelect().
                clickNext().
                setEmail("test@gmail.com").
                setPhone("123456789").
                setGetFreeTextMessage().
                setFirstName("TstFName").
                setLastName("TstLName").
                selectGender("Male").
                setDayOfBirth("25").
                selectMonthOfBirth("January").
                setYearOfBirth("1984").
                assertEmailText().
                assertPhoneText().
                assertFirstNameText().
                assertLastNameText().
                assertGenderText().
                assertDayText().
                assertMonthText().
                assertYearText();

    }

    @AfterEach
    public void tearDown() {
        driver.close();
        driver.quit();
    }
}
